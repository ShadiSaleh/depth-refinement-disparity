

import torch.nn.functional as F
import torch
import torch.nn as nn



class hypercolumn(nn.Module):
    def __init__(self, ngf = 16):
        super(hypercolumn, self).__init__()

        self.cn1_d = nn.Conv2d(1, ngf//2, 3, padding=1)
        self.cn1_c = nn.Conv2d(3, ngf//2, 3, padding=1)
        self.cn2 = nn.Conv2d(ngf, ngf, 3, padding=1)
        self.mp = nn.MaxPool2d(2, stride = 2)
        self.cn3 = nn.Conv2d(ngf*2, ngf*2, 3, padding=1) #ngf*2
        self.cn4 = nn.Conv2d(ngf*2, ngf*2, 3, padding=1)
        
        self.upscale = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.upscale2 = nn.Upsample(scale_factor=4, mode='bilinear', align_corners=True)
        
        self.mp2 = nn.MaxPool2d(2, stride = 2)        
        
        self.cn5 = nn.Conv2d(ngf*4, ngf * 4, 3, padding=1)
        self.cn6 = nn.Conv2d(ngf*4, ngf * 4, 3, padding=1)
        self.cn7 = nn.Conv2d(ngf*4, ngf * 4, 3, padding = 1)
    
        self.cn8 = nn.Conv2d(ngf*7, ngf, 1)
        self.cn9 = nn.Conv2d(ngf, ngf//4, 1)
        self.output = nn.Conv2d(ngf//4, 1, 1)
            
    def forward(self, depth, rgb):
        cnv1_d = F.leaky_relu(self.cn1_d(depth))
        cnv1_c = F.leaky_relu(self.cn1_c(rgb))
        
        cnv1 = torch.cat((cnv1_d, cnv1_c), dim=1)
        cnv2 = F.leaky_relu(self.cn2(cnv1))
        hyper1 = torch.cat((cnv1, cnv2), dim=1)  #ngf*2
        
        pool1 = self.mp(hyper1)
        cnv3 = F.leaky_relu(self.cn3(pool1))
        cnv4 = F.leaky_relu(self.cn4(cnv3))
        
        up1 = self.upscale(cnv4) #ngf = 2
        hyper2 = torch.cat([cnv3, cnv4], dim=1)  # ngf*4
        
        pool2 = self.mp2(hyper2)
        cnv5 = F.leaky_relu(self.cn5(pool2))
        cnv6 = F.leaky_relu(self.cn6(cnv5))
        cnv7 = F.leaky_relu(self.cn7(cnv6))
        up2 = self.upscale2(cnv7) #ngf = 4
        
        cat = torch.cat((cnv2, up1, up2), dim=1)  #ngf*7
        
        cnv8 = F.leaky_relu(self.cn8(cat))
        cnv9 = F.leaky_relu(self.cn9(cnv8))
        out = torch.tanh(self.output(cnv9)) 
        return out
#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import torch
import torchvision
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from PIL import Image
import matplotlib
from sklearn.model_selection import train_test_split
import re

from utils import *
from unet_model import UNet
from vgg import Vgg16
from torch.optim import Adam
import visdom
from losses import *
import imutils
import torch
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
from math import exp
from hyper import *
import itertools

def gaussian(window_size, sigma):
    gauss = torch.Tensor([exp(-(x - window_size//2)**2/float(2*sigma**2)) for x in range(window_size)])
    return gauss/gauss.sum()

def create_window(window_size, channel):
    _1D_window = gaussian(window_size, 1.5).unsqueeze(1)
    _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
    window = Variable(_2D_window.expand(channel, 1, window_size, window_size).contiguous())
    return window

def _ssim(img1, img2, window, window_size, channel, size_average = True):
    mu1 = F.conv2d(img1, window, padding = window_size//2, groups = channel)
    mu2 = F.conv2d(img2, window, padding = window_size//2, groups = channel)

    mu1_sq = mu1.pow(2)
    mu2_sq = mu2.pow(2)
    mu1_mu2 = mu1*mu2

    sigma1_sq = F.conv2d(img1*img1, window, padding = window_size//2, groups = channel) - mu1_sq
    sigma2_sq = F.conv2d(img2*img2, window, padding = window_size//2, groups = channel) - mu2_sq
    sigma12 = F.conv2d(img1*img2, window, padding = window_size//2, groups = channel) - mu1_mu2

    C1 = 0.01**2
    C2 = 0.03**2

    ssim_map = ((2*mu1_mu2 + C1)*(2*sigma12 + C2))/((mu1_sq + mu2_sq + C1)*(sigma1_sq + sigma2_sq + C2))

    if size_average:
        return ssim_map.mean()
    else:
        return ssim_map.mean(1).mean(1).mean(1)

class SSIM(torch.nn.Module):
    def __init__(self, window_size = 11, size_average = True):
        super(SSIM, self).__init__()
        self.window_size = window_size
        self.size_average = size_average
        self.channel = 1
        self.window = create_window(window_size, self.channel)

    def forward(self, img1, img2):
        (_, channel, _, _) = img1.size()

        if channel == self.channel and self.window.data.type() == img1.data.type():
            window = self.window
        else:
            window = create_window(self.window_size, channel)
            
            if img1.is_cuda:
                window = window.cuda(img1.get_device())
            window = window.type_as(img1)
            
            self.window = window
            self.channel = channel


        return _ssim(img1, img2, window, self.window_size, channel, self.size_average)

def ssim(img1, img2, window_size = 11, size_average = True):
    (_, channel, _, _) = img1.size()
    window = create_window(window_size, channel)
    
    if img1.is_cuda:
        window = window.cuda(img1.get_device())
    window = window.type_as(img1)
    
    return _ssim(img1, img2, window, window_size, channel, size_average)


#viz = visdom.Visdom()
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print(device)
# torch.cuda.set_device(1)
CONTENT_WEIGHT = 1e0
LEARNING_RATE = 0.001

def dialation_holes(hole_mask):
    b, ch, h, w = hole_mask.shape
    dilation_conv = nn.Conv2d(ch, ch, 3, padding=1, bias=False).to(device)
    torch.nn.init.constant_(dilation_conv.weight, 1.0)
    with torch.no_grad():
        output_mask = dilation_conv(hole_mask)
    updated_holes = output_mask != 0
    return updated_holes.float()

def total_variation_loss(image,mask):
    hole_mask = 1-mask
    dilated_holes=dialation_holes(hole_mask)
    colomns_in_Pset=dilated_holes[:, :, :, 1:] * dilated_holes[:, :, :, :-1]
    rows_in_Pset=dilated_holes[:, :, 1:, :] * dilated_holes[:, :, :-1:, :]
    loss = torch.sum(torch.abs(colomns_in_Pset*(image[:, :, :, 1:] - image[:, :, :, :-1]))) + \
        torch.sum(torch.abs(rows_in_Pset*(image[:, :, :1 :] - image[:, :, -1:, :])))
    return loss


def tv_loss(output, weight=0.1):
    mask=torch.ones(output.shape)
    tvLoss = weight*total_variation_loss(torch.mul(output, mask), mask).mean()
    return tvLoss

def tensor2im(input_image, imtype=np.uint8):

 """"Converts a Tensor array into a numpy image array.

 Parameters:
 input_image (tensor) -- the input image tensor array
 imtype (type) -- the desired type of the converted numpy array
 """
 if not isinstance(input_image, np.ndarray):
     if isinstance(input_image, torch.Tensor): # get the data from a variable
         image_tensor = input_image.data
     else:
         return input_image
     image_numpy = image_tensor[0].cpu().float().numpy() # convert it into a numpy array
     if image_numpy.shape[0] == 1: # grayscale to RGB
         image_numpy = np.tile(image_numpy, (3, 1, 1))
     image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + 1) / 2.0 * 255.0
 else: # if it is a numpy array, do nothing
     image_numpy = input_image
 return image_numpy.astype(imtype)

def create_vis_plotter(viz, _xlabel, _ylabel, _title, _legend):
    return viz.line(
            X=torch.zeros((1,)).cpu(),
            Y=torch.zeros((1, 2)).cpu(),
            opts=dict(
                    xlabel=_xlabel,
                    ylabel=_ylabel,
                    title=_title,
                    legend=_legend
                    )
            )

def update_vis_plotter(viz, iteration, loc, conf, window1, update_type):
    viz.line(
            X=torch.ones((1, 2)).cpu() * iteration,
            Y=torch.Tensor([loc, conf]).unsqueeze(0).cpu(),
            win=window1,
            update=update_type
            )


dep_root = 'rgb'#\\Automation\\Renders\\7-10-19'K:\zz-dataset\TrackView2
inp_root = 'depth'


x_inp = os.listdir(inp_root)
rgb_images = os.listdir(dep_root)
x_inp.sort()
rgb_images.sort()
# print(x_inp, rgb_images)
# import sys
# sys.exit()


# x_inp = ['Cardiff_Uph_Chair_Arm_Persp_EverydaySuedeLightWheat_disp_5_7.jpg']
# rgb_images = ['Cardiff_Uph_Chair_Arm_Persp_EverydaySuedeLightWheat0005.exr']

          
image_size = 1024  
batch_size_val = 1


model_unet = UNet(1, 1).to(device)
model_hp =hypercolumn(16).to(device)
model_unet.load_state_dict(torch.load('model_unet_0730_5.pth'))
model_hp.load_state_dict(torch.load('model_hypercol_0730_5.pth'))

optimizer = torch.optim.Adam(itertools.chain(model_unet.parameters(), model_hp.parameters()), lr=LEARNING_RATE, betas=(0.9, 0.999))
# model_unet.load_state_dict(torch.load('model_depth_unet_3.pth'))
# model_hp.load_state_dict(torch.load('model_depth_hypercol_3.pth'))




model_unet.eval()
model_hp.eval()

dtype = torch.cuda.FloatTensor

for i in range(len(rgb_images)):
    print(rgb_images[i], rgb_images[i].split('.')[1])

    if(rgb_images[i].split('.')[1] == 'exr'):
        imgs = exr2png(os.path.join(dep_root,rgb_images[i]))[:,:,:3]
        imgs = cv2.resize(imgs, (image_size,image_size))
        imgs = np.clip(imgs,0,1)
        imgs = (255*(imgs**(1/2.4))).astype('uint8')
        img = cv2.cvtColor(imgs,cv2.COLOR_BGR2GRAY)
        ima=(img>0).astype('uint8')
    else:
        imgs = cv2.imread(os.path.join(dep_root,rgb_images[i]))
        img = cv2.cvtColor(imgs,cv2.COLOR_BGR2GRAY)
        ima = np.where(img > 50, img, 0)

        # plt.imshow(ima[::-1, :])

        # plt.imshow(ima[::-1,:], cmap = 'gray')
        # plt.show()
        # import sys
        # sys.exit

    _, contours, hierarchy = cv2.findContours(ima.copy(), cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key = cv2.contourArea, reverse = True)
    c_0 = contours[0]
    x, y, w, h = cv2.boundingRect(c_0)
    im_inp = imgs[y:y+h, x:x+w,:]
    coor1 = int(np.ceil(im_inp.shape[0] / 4) * 4)
    coor2 = int(np.ceil(im_inp.shape[1] / 4) * 4)
    temp = np.zeros([coor1, coor2, 3]).astype('uint8')
    temp[:im_inp.shape[0], :im_inp.shape[1], :] = im_inp.astype('uint8')

    data_transforms_rgb = torchvision.transforms.Compose([
                    torchvision.transforms.ToTensor(),
                    torchvision.transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5))
                ])

    data_transforms_inp = torchvision.transforms.Compose([
                    torchvision.transforms.ToTensor(),
                    torchvision.transforms.Normalize((0.5,), (0.5,))
                ])



    if(rgb_images[i].split('.')[1] != 'exr'):
        rgb_tensor = (data_transforms_inp(Image.fromarray(temp[::-1, :])).float()).unsqueeze(0)
    else:
        rgb_tensor = (data_transforms_inp(Image.fromarray(temp)).float()).unsqueeze(0)
    # rgb_tensor = (data_transforms_rgb(Image.fromarray(temp)).float()).unsqueeze(0)

    # depth_img_name = re.findall('\d+', rgb_images[i])[0]

    print(x_inp[i])

    inp = cv2.imread(os.path.join(inp_root, x_inp[i]),0)
    im_inp = inp[y:y+h, x:x+w]
    coor1 = int(np.ceil(im_inp.shape[0] / 4) * 4)
    coor2 = int(np.ceil(im_inp.shape[1] / 4) * 4)
    temp = np.zeros([coor1, coor2]).astype('uint8')
    temp[:im_inp.shape[0], :im_inp.shape[1]] = im_inp.astype('uint8')


    if(rgb_images[i].split('.')[1] != 'exr'):
        input_tensor = (data_transforms_inp(Image.fromarray(temp[::-1, :])).float()).unsqueeze(0)
    else:
        input_tensor = (data_transforms_inp(Image.fromarray(temp)).float()).unsqueeze(0)


    # print(rgb_tensor.shape, input_tensor.shape)


    img = Variable(input_tensor).to(device)
    rgb_inp = Variable(rgb_tensor).to(device)

    predicted = model_unet(img)
    # print(predicted.shape)
    clean_depth=model_hp(predicted,rgb_inp)
    # print(clean_depth.shape)

    # img = predicted[0].squeeze(0) / 2 + 0.5     
    # npimg = img.detach().cpu().numpy()
    # matplotlib.image.imsave('final/' + rgb_images[i].split('.')[0]+'_pred-dep.jpg', npimg, cmap = 'gray')


    img = clean_depth[0].squeeze(0) / 2 + 0.5     
    npimg = img.detach().cpu().numpy()
    matplotlib.image.imsave('final/' + rgb_images[i].split('.')[0]+'_clean-dep.jpg', npimg, cmap = 'gray')


    # img = rgb_inp[0] / 2 + 0.5     
    # npimg = img.detach().cpu().numpy()
    # matplotlib.image.imsave(rgb_images[i].split('.')[0] + '_rgb-cut.jpg', np.transpose(npimg,(1,2,0)))

    # break






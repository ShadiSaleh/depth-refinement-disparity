# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 17:24:27 2019

@author: aadishesha
"""
import sys
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable, Function

#from ipdb import set_trace as st
import numpy as np

#from .rank_loss import image_rank_4d


def _assert_no_grad(variable):
    assert not variable.requires_grad, \
        "nn criterions don't compute the gradient w.r.t. targets - please " \
        "mark these variables as volatile or not requiring gradients"

class MSEScaledError(nn.Module):
    def __init__(self):
        super(MSEScaledError, self).__init__()

    def forward(self, input, target, no_mask=True):
        # input_meters = ((input + 1) / 2) * self.scale_to_meters
        # target_meters = ((target + 1) / 2) * self.scale_to_meters

        error = input - target

        # print(torch.sum(error * error))
        # print(mask.sum().type(torch.FloatTensor))
        # print(mask.type(torch.FloatTensor).sum())
        # print((mask >= 0.0).sum())
        if no_mask:
            return torch.mean(error * error).cuda()
        else:
            return (torch.sum(error * error)) / mask.sum().type(torch.FloatTensor).cuda()


class EigenLoss(nn.Module):
    def __init__(self):
        super(EigenLoss, self).__init__()

    def _data_in_log_meters(self, data):
        """ log((0.5 * (data + 1)) * 10) in meters"""
        # data is between [-1, 1]
        # we are going to apply log, but according to the original distance
        return torch.log(5 * (data + 1))

    def forward(self, input, target):
        _assert_no_grad(target)
        # get data in log meters
        log_input, log_target = self._data_in_log_meters(input), self._data_in_log_meters(target)
#        print (log_input, log_target)
        # number of elements
        n_el = log_input.data.numel()
#        print(n_el,"elem")
        error = log_input - log_target

        # L2 loss
        loss1 = torch.mean(error * error)

        # scale invariant difference
        mean_error = torch.mean(error)
        loss2 = (mean_error * mean_error) * 0.5 / (n_el * n_el)
#        print (mean_error,"meanerror")
        loss = loss1 - loss2
        
        return loss
    
class HuberLoss(nn.Module):
    """Adds a Huber Loss term to the training procedure.
    For each value x in `error=labels-predictions`, the following is calculated:
    ```
    0.5 * x^2                  if |x| <= d
    0.5 * d^2 + d * (|x| - d)  if |x| > d
    ```
    """

    def __init__(self):
        super(HuberLoss, self).__init__()

    def forward(self, input, target):  # delta changes everytime - per batch
        # all variables here must be torch.autograd.Variable to perform autograd
        _assert_no_grad(target)
        error = (target - input)
        absError = torch.abs(error)

        delta = 0.2 * torch.max(absError).item()

        ft1 = 0.5 * error * error
        ft2 = 0.5 * delta * delta + delta * (absError - delta)

        mask_down_f = absError.le(delta).float()
        mask_up_f = absError.gt(delta).float()

        loss = ft1 * mask_down_f + ft2 * mask_up_f

        return torch.mean(loss)
    
    



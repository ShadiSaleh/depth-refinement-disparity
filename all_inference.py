#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import torch
import torchvision
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from PIL import Image
import matplotlib
from sklearn.model_selection import train_test_split
import re

from utils import *
from unet_model import UNet
from vgg import Vgg16
from torch.optim import Adam
import visdom
from losses import *
import imutils
import torch
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
from math import exp
from hyper import *
import itertools

def gaussian(window_size, sigma):
    gauss = torch.Tensor([exp(-(x - window_size//2)**2/float(2*sigma**2)) for x in range(window_size)])
    return gauss/gauss.sum()

def create_window(window_size, channel):
    _1D_window = gaussian(window_size, 1.5).unsqueeze(1)
    _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
    window = Variable(_2D_window.expand(channel, 1, window_size, window_size).contiguous())
    return window

def _ssim(img1, img2, window, window_size, channel, size_average = True):
    mu1 = F.conv2d(img1, window, padding = window_size//2, groups = channel)
    mu2 = F.conv2d(img2, window, padding = window_size//2, groups = channel)

    mu1_sq = mu1.pow(2)
    mu2_sq = mu2.pow(2)
    mu1_mu2 = mu1*mu2

    sigma1_sq = F.conv2d(img1*img1, window, padding = window_size//2, groups = channel) - mu1_sq
    sigma2_sq = F.conv2d(img2*img2, window, padding = window_size//2, groups = channel) - mu2_sq
    sigma12 = F.conv2d(img1*img2, window, padding = window_size//2, groups = channel) - mu1_mu2

    C1 = 0.01**2
    C2 = 0.03**2

    ssim_map = ((2*mu1_mu2 + C1)*(2*sigma12 + C2))/((mu1_sq + mu2_sq + C1)*(sigma1_sq + sigma2_sq + C2))

    if size_average:
        return ssim_map.mean()
    else:
        return ssim_map.mean(1).mean(1).mean(1)

class SSIM(torch.nn.Module):
    def __init__(self, window_size = 11, size_average = True):
        super(SSIM, self).__init__()
        self.window_size = window_size
        self.size_average = size_average
        self.channel = 1
        self.window = create_window(window_size, self.channel)

    def forward(self, img1, img2):
        (_, channel, _, _) = img1.size()

        if channel == self.channel and self.window.data.type() == img1.data.type():
            window = self.window
        else:
            window = create_window(self.window_size, channel)
            
            if img1.is_cuda:
                window = window.cuda(img1.get_device())
            window = window.type_as(img1)
            
            self.window = window
            self.channel = channel


        return _ssim(img1, img2, window, self.window_size, channel, self.size_average)

def ssim(img1, img2, window_size = 11, size_average = True):
    (_, channel, _, _) = img1.size()
    window = create_window(window_size, channel)
    
    if img1.is_cuda:
        window = window.cuda(img1.get_device())
    window = window.type_as(img1)
    
    return _ssim(img1, img2, window, window_size, channel, size_average)


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print(device)

CONTENT_WEIGHT = 1e0
LEARNING_RATE = 0.001

viz = visdom.Visdom()


def create_vis_plot(viz, name):
 return viz.image((torch.zeros([1,256, 256])), opts={'title': name})

window1 = create_vis_plot(viz, 'GT')
window2 = create_vis_plot(viz, 'Pred')
window3 =create_vis_plot(viz, 'clean')
window4 =create_vis_plot(viz, 'Diff')
window5 =create_vis_plot(viz, 'Inp-Disp')
# window6 =create_vis_plot(viz, 'RGB')
#import sys
#sys.exit()


def dialation_holes(hole_mask):
    b, ch, h, w = hole_mask.shape
    dilation_conv = nn.Conv2d(ch, ch, 3, padding=1, bias=False).to(device)
    torch.nn.init.constant_(dilation_conv.weight, 1.0)
    with torch.no_grad():
        output_mask = dilation_conv(hole_mask)
    updated_holes = output_mask != 0
    return updated_holes.float()

def total_variation_loss(image,mask):
    hole_mask = 1-mask
    dilated_holes=dialation_holes(hole_mask)
    colomns_in_Pset=dilated_holes[:, :, :, 1:] * dilated_holes[:, :, :, :-1]
    rows_in_Pset=dilated_holes[:, :, 1:, :] * dilated_holes[:, :, :-1:, :]
    loss = torch.sum(torch.abs(colomns_in_Pset*(image[:, :, :, 1:] - image[:, :, :, :-1]))) + \
        torch.sum(torch.abs(rows_in_Pset*(image[:, :, :1 :] - image[:, :, -1:, :])))
    return loss


def tv_loss(output, weight=0.1):
    mask=torch.ones(output.shape)
    tvLoss = weight*total_variation_loss(torch.mul(output, mask), mask).mean()
    return tvLoss

def tensor2im(input_image, imtype=np.uint8):

 """"Converts a Tensor array into a numpy image array.

 Parameters:
 input_image (tensor) -- the input image tensor array
 imtype (type) -- the desired type of the converted numpy array
 """
 if not isinstance(input_image, np.ndarray):
     if isinstance(input_image, torch.Tensor): # get the data from a variable
         image_tensor = input_image.data
     else:
         return input_image
     image_numpy = image_tensor[0].cpu().float().numpy() # convert it into a numpy array
     if image_numpy.shape[0] == 1: # grayscale to RGB
         image_numpy = np.tile(image_numpy, (3, 1, 1))
     image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + 1) / 2.0 * 255.0
 else: # if it is a numpy array, do nothing
     image_numpy = input_image
 return image_numpy.astype(imtype)

def create_vis_plotter(viz, _xlabel, _ylabel, _title, _legend):
    return viz.line(
            X=torch.zeros((1,)).cpu(),
            Y=torch.zeros((1, 2)).cpu(),
            opts=dict(
                    xlabel=_xlabel,
                    ylabel=_ylabel,
                    title=_title,
                    legend=_legend
                    )
            )

def update_vis_plotter(viz, iteration, loc, conf, window1, update_type):
    viz.line(
            X=torch.ones((1, 2)).cpu() * iteration,
            Y=torch.Tensor([loc, conf]).unsqueeze(0).cpu(),
            win=window1,
            update=update_type
            )


dep_root = 'K:/zz-dataset/TrackView2'
inp_root1 = 'Q:/Demo/zz-data/disparities'
inp_root2 = 'Q:/Demo/zz-data/dispexr'

dirs = os.listdir(dep_root)
dirs.sort()
fin_dirs = []
for i, j in enumerate(dirs):
    print(i)
    if "Depth"in j:
        if "Ortho" not in j:
            fin_dirs.append(j)
fin_dirs.sort()


dic = {}
rgb_images = []
# exrs_disp = []

inps = os.listdir(inp_root1)
for i in inps:
    for j in fin_dirs:
        if j.split("_Depth")[0] in i:
            if os.path.exists(dep_root+'/'+j+"/"+j+re.findall('\d+', i)[0].zfill(4)+".exr"):
                dic[i] = j+"/"+j+re.findall('\d+', i)[0].zfill(4)+".exr" # disparity to gt depth map
                rgb_images.append(i.split("_disp")[0]+"/" + i.split("_disp")[0] +re.findall('\d+', i)[0].zfill(4)+".exr")
                # exrs_disp.append()
                break


x_inp = []
y_lbl = []
x_inp = list(dic.keys())
y_lbl = list(dic.values())
print(len(x_inp),len(y_lbl), len(rgb_images))


#import sys
#sys.exit()
          
image_size = 1024
batch_size_train = 1   
batch_size_val = 1


#xtrain, x_val_test, ytrain, y_val_test = train_test_split(x_inp, y_lbl, test_size=0.3)
#xval, xtest, yval, ytest = train_test_split(x_val_test, y_val_test, test_size=0.1)
#print(len(xtrain), len(xval), len(xtest))
#


# In[3]:


class DataLoader:
    def __init__(self, X, Y, rgb, batch_size):
        '''
        Parameters:
        
        '''
        self.device = device
        self.inp_jpg_path = inp_root1
        self.inp_exr_path = inp_root2
        self.out_dep_path = dep_root
        self.image_size = image_size
        self.batch_size = batch_size
        self.X = X
        self.Y = Y
        self.rgb_imgs = rgb
        self.data_transforms_lbl = torchvision.transforms.Compose([
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize((0.5,), (0.5,))
            ])
    
        self.data_transforms_inp = torchvision.transforms.Compose([
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize((0.5,), (0.5,))
            ])
    
        self.data_transforms_rgb = torchvision.transforms.Compose([
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5))
            ])
        

    def get_dep(self, dep_path, image_name):
        #print("dep", image_name)
        im = exr2png(os.path.join(dep_path, image_name))[:,:,:3]
        im = cv2.resize(im, (image_size,image_size))
        im = np.clip(im,0,1)
        im = (255*(im**(1/2.4))).astype('uint8')
        im = im[:,:,0]
        immask=(im>0).astype('uint8')
        output_tensor = self.data_transforms_lbl(Image.fromarray(im)).float()
        output_tensormask = torch.from_numpy(immask).float()
        return output_tensor,output_tensormask.unsqueeze(0)
    
    def get_cords(self, imgs):
        imgs=cv2.cvtColor(imgs,cv2.COLOR_BGR2GRAY)
        ima=(imgs>0).astype('uint8')
        _,contours, hierarchy = cv2.findContours(ima.copy(), cv2.RETR_TREE,
                                cv2.CHAIN_APPROX_SIMPLE)
        c = sorted(contours, key = cv2.contourArea, reverse = True)
        (xpos, ypos, wid, hi) = cv2.boundingRect(c[0])
        jack=[xpos,ypos,wid,hi]
        
        return jack
    
    def get_rgb(self, dep_path, image_name):
        im = exr2png(os.path.join(dep_path, image_name))[:,:,:3]
        im = cv2.resize(im, (image_size,image_size))
        im = np.clip(im,0,1)
        im = (255*(im**(1/2.4))).astype('uint8')
        indices=self.get_cords(im)
        rgb_tensor = self.data_transforms_rgb(Image.fromarray(im)).float()
        return rgb_tensor,indices
        
    def get_inp(self, inp_path, image_name):
        while True:
            try:
                inp = cv2.imread(os.path.join(inp_path, image_name),0)
                break
            except:
                pass
        inp = cv2.resize(inp, (image_size,image_size))
        input_tensor = self.data_transforms_inp(Image.fromarray(inp)).float()
        return input_tensor
    
    def disp_inp(self, img):
        img = img / 2 + 0.5     
        npimg = img.cpu().numpy()
        plt.figure(figsize = (4,4))
        plt.imshow(npimg, aspect='auto', cmap = 'gray')
    
    def winsizecalc(self,indi):
        #print(indi, "indices list all")
        jx=[]
        jy=[]
        jw=[]
        jh=[]
        for i in indi:
            w=i[0]+i[2]
            h=i[1]+i[3]
            jx.append(i[0])
            jy.append(i[1])
            jw.append(w)
            jh.append(h)
        minx=min(jx)
        miny=min(jy)
        maxw=max(jw)
        maxh=max(jh)
        #print(minx,miny,maxw,maxh)
        return (minx,miny,maxw,maxh)
        
    def disp_out(self, img):    
        img = img / 2 + 0.5   
        npimg = img.cpu().numpy()
        plt.figure(figsize = (4,4))
        plt.imshow(npimg, aspect='auto', cmap = 'gray')
        
    def disp_rgb(self, img):
        img = img / 2 + 0.5   
        npimg = img.cpu().numpy()
        plt.figure(figsize = (4,4))
        plt.imshow(np.transpose(npimg,(1,2,0)), aspect='auto')
        
    def disp_mask(self, img):      
        npimg = img.cpu().numpy()
        plt.figure(figsize = (4,4))
        plt.imshow(npimg, aspect='auto', cmap = 'gray')
        
    def data_generator(self):
        while True:
            x, y, z, ind= [],[],[],[]
            tempx, tempy, tempz = [],[],[]
            
            idx = np.random.choice(np.arange(len(self.X)), self.batch_size)
            
            for i in range(len(idx)):
                if(np.random.choice(2, 1)[0] == 0):
                    inp_jpg = self.get_inp(self.inp_jpg_path, self.X[idx[i]])
                    x.append(inp_jpg)
                else:
                    inp_exr = self.get_inp(self.inp_exr_path, self.X[idx[i]])
                    x.append(inp_exr)

                lbl,mask = self.get_dep(self.out_dep_path, self.Y[idx[i]])
                rgb_i,indices = self.get_rgb(self.out_dep_path, self.rgb_imgs[idx[i]])
                
                # x.append(inp)
                y.append(lbl)
                z.append(rgb_i)
                ind.append(indices)
                
            ixx=self.winsizecalc(ind)
            #print (ixx)
            
            for r in range(0,len(x)):
                #print(x[r].shape)
                
                x[r] = x[r][:, ixx[1]:ixx[3], ixx[0]:ixx[2]]
                y[r] = y[r][:, ixx[1]:ixx[3], ixx[0]:ixx[2]]
                z[r] = z[r][:, ixx[1]:ixx[3], ixx[0]:ixx[2]]
                
                coor1 = int(np.ceil(x[r].shape[1] / 4) * 4)
                coor2 = int(np.ceil(x[r].shape[2] / 4) * 4)
                
                tempx.append(torch.zeros([1, coor1, coor2]))
                tempx[r][:, :x[r].shape[1], :x[r].shape[2]] = x[r]
                
                tempy.append(torch.zeros([1, coor1, coor2]))
                tempy[r][:, :y[r].shape[1], :y[r].shape[2]] = y[r]
                
                tempz.append(torch.zeros([3, coor1, coor2]))
                tempz[r][:, :z[r].shape[1], :z[r].shape[2]] = z[r]

            yield torch.stack(tempx), torch.stack(tempy),torch.stack(tempz)



data_train = DataLoader(x_inp, y_lbl, rgb_images, batch_size_train)
model_unet = UNet(1, 1).to(device)


model_hp =hypercolumn(16).to(device)
model_unet.load_state_dict(torch.load('model_unet_0730_2.pth'))
model_hp.load_state_dict(torch.load('model_hypercol_0730_2.pth'))
optimizer = torch.optim.Adam(itertools.chain(model_unet.parameters(), model_hp.parameters()), lr=LEARNING_RATE, betas=(0.9, 0.999))

model_unet.eval()
model_hp.eval()

dtype = torch.cuda.FloatTensor

vis_legend = ['Loss1', 'Loss2']
iter_plot = create_vis_plotter(viz, 'Iteration', 'Loss', 'Loss Plots', vis_legend)
itr=0
num_images = len(x_inp)
epoch_loss = 0


for epoch in range(0, 15):
    epoch_loss = 0
    for i in range(num_images // batch_size_train):
        itr+=1
        x, y, z = next(data_train.data_generator())
        img = Variable(x).to(device)
        label = Variable(y).to(device)
        rgb_inp = Variable(z).to(device)

        try: 
            predicted = model_unet(img)
            clean_depth=model_hp(predicted,rgb_inp)
        except:
            pass
        
        # if(itr % 3 == 0):
        viz.image(tensor2im((torch.cat((label,)*3,1))).transpose([2, 0, 1]), window1, opts={'title': 'GT'})
        viz.image(tensor2im((torch.cat((predicted,)*3,1))).transpose([2, 0, 1]), window2, opts={'title': 'Pred'})
        viz.image(tensor2im((torch.cat((clean_depth,)*3,1))).transpose([2, 0, 1]), window3, opts={'title': 'clean'})
        diff=torch.abs(torch.cat((clean_depth,)*3,1)-torch.cat((label,)*3,1))
        viz.image(tensor2im(diff - 1).transpose([2, 0, 1]), window4, opts={'title': 'Diff'})
        viz.image(tensor2im((torch.cat((img,)*3,1))).transpose([2, 0, 1]), window5, opts={'title': 'Inp-Disparity'})




